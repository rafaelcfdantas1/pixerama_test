@extends('_base')

@section('content')
    <section class="d-flex justify-content-center align-items-center vh-100">
        <form action="{{ route('login') }}"  method="post" class="login--form section mx-auto py-5 text-center rounded-4">
            @csrf
            <h1 class="fw-bold mb-5">Bem-Vindo</h1>
            @if ($errors->any())
                <p class="text-danger mb-3">
                    @foreach ($errors->all() as $error)
                        {{ $error }} <br>
                    @endforeach
                </p>
            @endif
            <input type="email" name="email" value="{{ old('email') }}" class="input input--login d-block mb-5 mx-auto w-75 {{ $errors->has('email') ? 'invalid' : '' }}" placeholder="Email" required>
            <input type="password" name="password" class="input input--login d-block mb-5 mx-auto w-75 {{ $errors->has('password') ? 'invalid' : '' }}" placeholder="Senha" required>
            <button type="submit" class="btn-primary d-block mx-auto w-75 rounded-pill border-0 py-3 text-white">LOGIN</button>
        </form>
    </section>
@endsection
