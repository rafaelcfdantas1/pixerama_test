@extends('_base')

@section('content')
    <nav class="nav px-4 my-4">
        <ul class="nav w-100 align-items-center">
            <li class="nav-item title">
                <h1 class="mb-0">Leads</h1>
            </li>

            <li class="nav-item me-auto">
                <a href="{{ url('/lead') }}" class="btn btn-primary text-white btn--icon"><i class="uil uil-user-plus"></i></a>
            </li>

            <li class="nav-item">
                <a href="{{ url('/logout') }}" class="btn btn-primary text-white btn--icon"><i class="uil uil-signout"></i></a>
            </li>
        </ul>

        <form action="{{ url('') }}" class="d-flex" autocomplete="off">
            <input type="search" name="q" class="input input--search me-2" placeholder="Busque por nome" aria-label="Search">
            <button class="btn btn--search me-2" type="submit">Buscar</button>
        </form>
    </nav>

    <section class="section rounded-4 p-5 table-responsive">
        <div id="alert-wrapper"></div>

        <table class="leads--table table table-hover">
            <thead>
                <tr>
                    <th scope="col" style="width: 35%">Nome</th>
                    <th scope="col" style="width: 15%">CPF</th>
                    <th scope="col" style="width: 25%">Email</th>
                    <th scope="col" style="width: 20%">Telefone</th>
                    <th scope="col" style="width: 5%">Ações</th>
                </tr>
            </thead>

            <tbody>
            @forelse ($leads as $lead)
                <tr>
                    <td>{{ $lead['nome'] }}</td>
                    <td>{{ $lead['cpf'] }}</td>
                    <td>{{ $lead['email'] }}</td>
                    <td>{{ $lead['telefone'] }}</td>
                    <td>
                        <section>
                            <a class="icon edit" href="{{ url('lead/' . $lead['id']) }}" role="button"><i class="uil uil-pen"></i></a>
                            <a class="icon delete" href="{{ url('lead/' . $lead['id']) }}" role="button"><i class="uil uil-trash-alt"></i></a>
                        </section>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="5">Nenhum lead cadastrado!</td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $leads->links() }}
    </section>

    <script type="text/javascript" src="{{ url('assets/js/delete_lead.js') }}"></script>
@endsection
