@extends('_base')

@section('content')
    <ul class="nav my-4 px-4 align-items-center">
        <li class="nav-item title">
            <h1 class="mb-0">Lead</h1>
        </li>

        <li class="nav-item me-auto">
            <a href="{{ url('') }}" class="btn btn-primary text-white btn--icon"><i class="uil uil-arrow-circle-left"></i></a>
        </li>

        <li class="nav-item">
            <a href="{{ url('/logout') }}" class="btn btn-primary text-white btn--icon"><i class="uil uil-signout"></i></a>
        </li>
    </ul>

    <section class="section rounded-4 p-5">
        <div id="alert-wrapper">
            @isset($alert)
                <x-alert :type="$alert['type']" :message="$alert['message']"/>
            @endisset
        </div>

        <form action="{{ url()->current() }}" method="POST" class="needs-validation" novalidate autocomplete="off">
            @csrf

            <div class="row">
                <div class="col-12 col-sm-6 col-md-12 mb-3">
                    <label for="nome" class="form-label">Nome*</label>
                    <input type="text" name="nome" value="{{ $lead['nome'] }}" class="form-control input letters nome {{ $errors->has('nome') ? 'is-invalid' : '' }}" id="nome" required>
                    <div class="invalid-feedback">{{ $errors->first('nome') }}</div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 mb-3">
                    <label for="cpf" class="form-label">CPF*</label>
                    <input type="text" name="cpf" value="{{ $lead['cpf'] }}" class="form-control input cpf {{ $errors->has('cpf') ? 'is-invalid' : '' }}" id="cpf" required>
                    <div class="invalid-feedback">{{ $errors->first('cpf') }}</div>
                </div>

                <div class="col-12 col-sm-6 col-md-3 mb-3">
                    <label for="telefone" class="form-label">Telefone*</label>
                    <input type="text" name="telefone" value="{{ $lead['telefone'] }}" class="form-control input telefone {{ $errors->has('telefone') ? 'is-invalid' : '' }}" id="telefone" required>
                    <div class="invalid-feedback">{{ $errors->first('telefone') }}</div>
                </div>

                <div class="col-12 col-sm-6 col-md-6 mb-3">
                    <label for="email" class="form-label">Email*</label>
                    <input type="email" name="email" value="{{ $lead['email'] }}" class="form-control input email {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" required>
                    <div class="invalid-feedback">{{ $errors->first('email') }}</div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-6 mb-3">
                    <label for="cep" class="form-label">CEP</label>
                    <input type="text" name="cep" value="{{ $lead['cep'] }}" class="form-control input cep" id="cep" data-old-value="{{ $lead['cep'] }}">
                </div>

                <div class="col-12 col-sm-6 mb-3">
                    <label for="rua" class="form-label">Rua</label>
                    <input type="text" name="rua" value="{{ $lead['rua'] }}" class="form-control input {{ $errors->has('rua') ? 'is-invalid' : '' }}" id="rua" @if(!$errors->has('rua')) readonly @endif>
                    <div class="invalid-feedback">{{ $errors->first('rua') }}</div>
                </div>

                <div class="col-12 col-sm-6 mb-3">
                    <label for="cidade" class="form-label">Cidade</label>
                    <input type="text" name="cidade" value="{{ $lead['cidade'] }}" class="form-control input {{ $errors->has('cidade') ? 'is-invalid' : '' }}" id="cidade" @if(!$errors->has('cidade')) readonly @endif>
                    <div class="invalid-feedback">{{ $errors->first('cidade') }}</div>
                </div>

                <div class="col-12 col-sm-6 mb-3">
                    <label for="estado" class="form-label">Estado</label>
                    <input type="text" name="estado" value="{{ $lead['estado'] }}" class="form-control input {{ $errors->has('estado') ? 'is-invalid' : '' }}" id="estado" @if(!$errors->has('estado')) readonly @endif>
                    <div class="invalid-feedback">{{ $errors->first('estado') }}</div>
                </div>
            </div>

            <div id="formHelp" class="form-text mb-3">* Campos obrigatórios</div>

            <button type="submit" class="btn btn-primary d-block mx-auto w-75 border-0 py-3 text-white">Salvar</button>
        </form>
    </section>

    <script type="text/javascript" src="{{ asset('assets/js/form_validation.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/cep.js') }}"></script>
@endsection
