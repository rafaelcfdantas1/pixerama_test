$(function () {
    handlerApiViaCep();
});

function handlerApiViaCep() {
    $('.cep').on('blur', function (e) {
        const cepField = $(this);
        const cepValue = cepField.val();
        const oldValue = cepField.attr('data-old-value');

        /**
         * Define o valor do 'field' com base no retorno da API e o bloqueia para edição.
         * Se a API não retornar valor, o 'field' fica vazio e livre para edição.
         *
         * Regra feita para combater ceps sem especificidade completa
         * (e.g. 29260-000 que representa a cidade de Domingos Martins)
         *
         * @param field
         * @param value
         */
        const validateField = (field, value) => {
            if (value) {
                field.val(value).prop('readonly', true)
            } else {
                field.val('').prop('readonly', false)
            }
        }

        // Só chama a API se o valor atual do cep for diferente do último digitado, evitando chamadas desnecessárias
        if (cepValue != oldValue) {
            // Regra para evitar uma chamada desnecessária à API
            // caso o usuário digite um cep válido e depois apague o valor do campo
            if (cepValue != '') {
                axios.get('https://viacep.com.br/ws/' + cepValue + '/json/').then(function (res) {
                    validateField($('#rua'), res.data.logradouro);
                    validateField($('#cidade'), res.data.localidade);
                    validateField($('#estado'), res.data.uf);

                    if (!res.data.erro) {
                        cepField.attr('data-old-value', cepValue);
                    } else {
                        cepField.val('').attr('data-old-value', '');
                    }
                })
            } else {
                cepField.val('').attr('data-old-value', '');
                $('#rua').val('').prop('readonly', true);
                $('#cidade').val('').prop('readonly', true);
                $('#estado').val('').prop('readonly', true);
            }

        }
    })
}
