$(function () {
    deleteLead();
});

function deleteLead() {
    $('.leads--table .delete').unbind('click').click(function (e) {
        e.preventDefault();
        const deleteButton = $(this);

        // Modal de confirmação de ação. Só executará o ajax caso o usuário confirme sua intenção.
        openModal('Tem certeza de que deseja excluir esta informação? Essa ação não poderá ser desfeita.', 'Atenção', true);

        $('#modal-default .btn-primary').click(function (e) {
            $.ajax({
                url: deleteButton.attr('href'),
                type: "DELETE",
                dataType: "json",
                data: {'_token': $("meta[name='csrf-token']").attr("content")},
                error: function (data) {
                    openModal('Parece que algo deu errado com a sua requisição. Tente novamente mais tarde!', 'Erro');
                },
                success: function (data) {
                    $('#modal-default [data-bs-dismiss="modal"]').click();
                    addAlert('success', 'O lead foi excluído com sucesso!');

                    deleteButton.closest('tr').remove();

                    if (!$('table tbody tr').length) {
                        $('table tbody').html('<tr><td colspan="5">Nenhum lead cadastrado!</td></tr>');
                    }
                }
            });
        })
    })
}
