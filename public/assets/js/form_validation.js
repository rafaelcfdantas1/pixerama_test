$(function () {
    validateForm();
    applyMasks();
});

function validateForm() {
    $('form.needs-validation').off('submit').on('submit', function (e) {
        // Validação de formulário client-side. Se falhar em alguma regra, não envia o formulário.
        if (isNomeInvalid() | isCpfInvalid() | isTelefoneInvalid() | isEmailInvalid()) {
            e.preventDefault();
            e.stopPropagation();
            return false;
        }

        return true;
    });
}

function applyMasks() {
    $('.cep').mask('00000-000', {clearIfNotMatch: true});
    $('.cpf').mask('000.000.000-00');
    $('.letters').mask('S', {translation: {'S': {pattern: /[a-zA-Z\s]/, recursive: true}}});

    var telefoneBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    }

    var telefoneOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(telefoneBehavior.apply({}, arguments), options);
        }
    };

    $('.telefone').mask(telefoneBehavior, telefoneOptions);
}

function isNomeInvalid() {
    let isInvalid = false;

    /**
     * Regras de validação:
     *
     * * Campo obrigatório
     *
     * @param field
     */
    const validate = (field) => {
        const makeInvalid = (msg) => {
            field.addClass('is-invalid')
                .removeClass('is-valid')
                .siblings('.invalid-feedback')
                .text(msg);
            isInvalid = true;
        }

        if (!field.val()) {
            makeInvalid('Campo obrigatório');
        } else {
            field.removeClass('is-invalid').addClass('is-valid');
        }
    }

    $('input.nome').each(function(){
        validate($(this));
    });

    return isInvalid;
}

function isCpfInvalid() {
    let isInvalid = false;

    /**
     * Regras de validação:
     *
     * * Campo obrigatório
     * * Formato xxx.xxx.xxx-xx
     *
     * @param field
     */
    const validate = (field) => {
        const makeInvalid = (msg) => {
            field.addClass('is-invalid')
                .removeClass('is-valid')
                .siblings('.invalid-feedback')
                .text(msg);
            isInvalid = true;
        }

        if (!field.val()) {
            makeInvalid('Campo obrigatório');
        } else if (field.val().length < 14) {
            makeInvalid('Formato xxx.xxx.xxx-xx');
        } else {
            field.removeClass('is-invalid').addClass('is-valid');
        }
    }

    $('input.cpf').each(function(i){
        validate($(this));
    });

    return isInvalid;
}

function isTelefoneInvalid() {
    let isInvalid = false;

    /**
     * Regras de validação:
     *
     * * Campo obrigatório
     * * Formato (xx) xxxx-xxxx
     * * Formato (xx) xxxxx-xxxx
     *
     * @param field
     */
    const validate = (field) => {
        const makeInvalid = (msg) => {
            field.addClass('is-invalid')
                .removeClass('is-valid')
                .siblings('.invalid-feedback')
                .text(msg);
            isInvalid = true;
        }

        if (!field.val()) {
            makeInvalid('Campo obrigatório');
        } else if (field.val().length < 14) {
            makeInvalid('Formato (xx) xxxx-xxxx ou (xx) xxxxx-xxxx');
        } else {
            field.removeClass('is-invalid').addClass('is-valid');
        }
    }

    $('input.telefone').each(function(i){
        validate($(this));
    });

    return isInvalid;
}

function isEmailInvalid() {
    let isInvalid = false;

    /**
     * Regras de validação:
     *
     * * Campo obrigatório
     * * Formato de email válido
     *
     * @param field
     */
    const validate = (field) => {
        const makeInvalid = (msg) => {
            field.addClass('is-invalid')
                .removeClass('is-valid')
                .siblings('.invalid-feedback')
                .text(msg);
            isInvalid = true;
        }

        /**
         * Sugestão de RegEx da W3C: https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address
         * Regexplained: http://tinyurl.com/mlwavo8
         */
        let regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

        if (!field.val()) {
            makeInvalid('Campo obrigatório');
        } else if (!regex.test(field.val())) {
            makeInvalid('Formato de email inválido');
        } else {
            field.removeClass('is-invalid').addClass('is-valid');
        }
    }

    $('input.email').each(function(i){
        validate($(this));
    });

    return isInvalid;
}
