<a id="readme-top"></a>



# Sistema de Gerenciamento de Leads

<img src="project.png" alt="Projeto">

> Este projeto é uma solução de um desafio técnico que abrange tanto o desenvolvimento front-end quanto o back-end, bem como a integração com APIs e a manipulação de dados.

<a href="./REQUISITOS.md">REQUISITOS</a>



<!-- SUMÁRIO -->
<details>
  <summary>Sumário</summary>
  <ol>
    <li><a href="#tecnologias">Tecnologias</a></li>
    <li><a href="#pré-requisitos">Pré-requisitos</a></li>
    <li><a href="#instalação">Instalação</a></li>
    <li><a href="#uso">Uso</a></li>
    <li><a href="#contato">Contato</a></li>
  </ol>
</details>



<!-- TECNOLOGIAS -->
## Tecnologias

* [![HTML][HTML-badge]][HTML-url]
* [![CSS][CSS-badge]][CSS-url]
* [![Bootstrap][Bootstrap-badge]][Bootstrap-url]
* [![JavaScript][JavaScript-badge]][JavaScript-url]
* [![jQuery][jQuery-badge]][jQuery-url]
* [![Axios][Axios-badge]][Axios-url]
* [![Laravel][Laravel-badge]][Laravel-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- PRÉ-REQUISITOS -->
## Pré-requisitos

* Git
* Docker

<p align="right">(<a href="#readme-top">voltar para o topo</a>)</p>



<!-- INSTALAÇÃO -->
## Instalação

Utilize o Prompt de Comando para o Windows e o Terminal padrão para o Linux.

1. Clone o repositório no destino de sua preferência e entre na pasta do laradock
   ```sh
   git clone https://gitlab.com/rafaelcfdantas1/pixerama_test.git
   cd pixerama_test/laradock
   ```
2. Caso seu sistema operacional seja Linux, abra o arquivo .env e altere o valor
   da diretiva COMPOSE_PATH_SEPARATOR para ``:``
3. Inicialize o Docker (esse processo pode demorar alguns minutos, pois será criado um container Workspace com várias utilidades) e entre no container do MySQL
   ```sh
   docker-compose up -d apache2 mysql
   docker-compose exec mysql bash
   ```
4. Execute o seguinte comando para criar o banco de dados. Se solicitar senha, utilize ``root``
   ```sh
   mysql -u root -p < /docker-entrypoint-initdb.d/createdb.sql
   ```
5. Saia do container do MySQL com o comando ``exit`` e entre no container do workspace
   ```sh
   docker-compose exec workspace bash
   ```
6. Instale as dependências do composer, execute as migrations, execute os seeders e saia do container
   com o comando ``exit``
   ```sh
   composer install
   php artisan db:seed
   php artisan migrate
   ```

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- USO -->
## Uso

* Acesse o projeto na URL `http://localhost/`.
* Para realizar login no sistema, utilize o email `admin@example.com` e a senha `admin`.
* Para executar os testes, execute `php artisan test`.

Lembre-se que qualquer comando do `artisan`, `composer` ou `php` deverá ser executado dentro do container workspace.
Na pasta laradock, execute `docker-compose exec workspace bash` para acessar o terminal do container.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- CONTATO -->
## Contato

[![GitHub][GitHub-badge]][GitHub-url]
[![LinkedIn][LinkedIn-badge]][LinkedIn-url]
[![Gmail][Gmail-badge]][Gmail-url]
[![WhatsApp][WhatsApp-badge]][WhatsApp-url]

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://github.com/Ileriayo/markdown-badges -->

<!-- Social -->
[GitHub-badge]: https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white
[GitHub-url]: https://github.com/rafaelcfdantas/
[LinkedIn-badge]: https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white
[LinkedIn-url]: https://www.linkedin.com/in/rafael-dantas-2019/
[Gmail-badge]: https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white
[Gmail-url]: mailto:rafael.cfd1999@gmail.com
[WhatsApp-badge]: https://img.shields.io/badge/WhatsApp-25D366?style=for-the-badge&logo=whatsapp&logoColor=white
[WhatsApp-url]: https://wa.me/5527992796965

<!-- Tecnologias -->
[HTML-badge]: https://img.shields.io/badge/html-white.svg?style=for-the-badge&logo=html5&logoColor=E34F26
[HTML-url]: https://developer.mozilla.org/pt-BR/docs/Web/HTML
[CSS-badge]: https://img.shields.io/badge/CSS-white?&style=for-the-badge&logo=css3&logoColor=blue
[CSS-url]: https://developer.mozilla.org/pt-BR/docs/Web/CSS
[SASS-badge]: https://img.shields.io/badge/Sass-CC6699?style=for-the-badge&logo=sass&logoColor=white
[SASS-url]: https://sass-lang.com/
[JavaScript-badge]: https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E
[JavaScript-url]: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript
[Bootstrap-badge]: https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com
[Axios-badge]: https://img.shields.io/badge/Axios-ffffff?style=for-the-badge&logo=axios&logoColor=5A29E4
[Axios-url]: https://axios-http.com/
[jQuery-badge]: https://img.shields.io/badge/jquery-%230769AD.svg?style=for-the-badge&logo=jquery&logoColor=white
[jQuery-url]: https://jquery.com/
[Laravel-badge]: https://img.shields.io/badge/laravel-%23FF2D20.svg?style=for-the-badge&logo=laravel&logoColor=white
[Laravel-url]: https://laravel.com/
