CREATE DATABASE IF NOT EXISTS `pixerama_test` COLLATE 'utf8_general_ci' ;
CREATE USER 'pixerama'@'%' IDENTIFIED BY 'pixerama';
GRANT ALL ON `pixerama_test`.* TO 'pixerama'@'%' ;
FLUSH PRIVILEGES ;
