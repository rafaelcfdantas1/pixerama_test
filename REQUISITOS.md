## Requisitos do desafio

* Front-End:
    - Tela de login
    - Crie uma interface de usuário responsiva para o sistema usando HTML, Bootstrap, CSS e JavaScript (jquery).
    - Implemente a página de listagem dos leads com a capacidade de adicionar, editar e excluir registros.

* Back-End:
    - Utilizar as ferramentas do Laravel de autenticação
    - Desenvolva utilizando os padrões do framework Laravel e Implemente APIs para:
    - Criar, ler, atualizar e excluir registros.

* Persistência de Dados:
    - Utilize um banco de dados (MySQL) para armazenar informações.
    - Modele as tabelas ou documentos necessários para representar essas entidades.

* Segurança:
    - Implemente autenticação e autorização básicas para proteger as operações sensíveis do sistema.
    - Certifique-se de que senhas sejam armazenadas de forma segura.

* Testes:
    - Escreva testes unitários para as principais funcionalidades do sistema.
    - Realize testes de integração para garantir que os componentes interajam corretamente.

Obs: Não há um layout base para ser seguido
