<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Lead>
 */
class LeadFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nome'     => fake()->name(),
            'cpf'      => fake()->cpf(),
            'telefone' => fake()->phoneNumber(),
            'email'    => fake()->safeEmail(),
            'cep'      => fake()->postcode(),
            'rua'      => fake()->streetName(),
            'cidade'   => fake()->city(),
            'estado'   => fake()->stateAbbr(),
        ];
    }
}
