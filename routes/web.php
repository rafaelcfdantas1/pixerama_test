<?php

use App\Http\Controllers\LeadsController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth')->group(function () {
    /**
     * Rota para listar os leads
     */
    Route::get('/', [LeadsController::class, 'get'])->name('leads');

    /**
     * Rota para o formulário de um lead
     */
    Route::get('/lead/{id?}', [LeadsController::class, 'read']);

    /**
     * Rota para criar ou editar um lead
     */
    Route::post('/lead/{id?}', [LeadsController::class, 'post']);

    /**
     * Rota para deletar um lead
     */
    Route::delete('/lead/{id}', [LeadsController::class, 'delete']);
});

/**
 * Rota para formulário de login
 */
Route::get('/login', [LoginController::class, 'login'])->name('login');

/**
 * Rota para autenticar o login
 */
Route::post('/login', [LoginController::class, 'authenticate']);

/**
 * Rota para realizar o logout
 */
Route::get('/logout', [LoginController::class, 'logout']);
