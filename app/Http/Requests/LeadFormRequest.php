<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadFormRequest extends FormRequest
{
    /**
     * Obtém as regras de validação para aplicar à requisição
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nome'     => ['required'],
            'cpf'      => ['required', 'regex:/([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/'],
            'telefone' => ['required', 'regex:/^\([1-9]{2}\) (?:[2-8]|9[0-9])[0-9]{3}\-[0-9]{4}$/'],
            'email'    => ['required', 'email'],
            'cep'      => ['nullable', 'regex:/\d{5}-\d{3}/'],
            'rua'      => ['required_with:cep'],
            'cidade'   => ['required_with:cep'],
            'estado'   => ['required_with:cep']
        ];
    }

    /**
     * Obtém as mensagens de erro para as regras de validação.
     *
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'required'          => 'Campo obrigatório',
            'email'             => 'Deve ser um email válido',
            'regex'             => 'Formato inválido',
            'required_with:cep' => 'Campo obrigatório quando cep está presente',
        ];
    }
}
