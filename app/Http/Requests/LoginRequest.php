<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Obtém as regras de validação para aplicar à requisição
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => ['required', 'email'],
            'password' => ['required'],
        ];
    }

    /**
     * Obtém um nome customizado para o campo definido
     *
     * @return array
     */
    public function attributes(): array
    {
        return [
            'password' => 'senha'
        ];
    }

    /**
     * Obtém as mensagens de erro para as regras de validação.
     *
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'required' => 'O campo :attribute é obrigatório',
            'email' => 'O campo :attribute deve ser um email válido'
        ];
    }
}
