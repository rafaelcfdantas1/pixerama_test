<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeadFormRequest;
use App\Models\Lead;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LeadsController extends Controller
{
    /**
     * "GET /"
     *
     * Lista de leads
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     * @throws \Exception
     */
    public function get()
    {
        $leads = Lead::orderBy('id', 'desc');

        if (request()->has('q')) {
            $leads->where('nome', 'like', '%' . request()->input('q') . '%');
        }

        return view('list', ['leads' => $leads->paginate(20)->withQueryString()]);
    }

    /**
     * "GET /lead/{id?}"
     *
     * Formulário para criar ou editar um lead
     *
     * @param Request $request
     * @param $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function read(Request $request, $id = null)
    {
        $data['lead'] = !empty($request->old()) ? $request->old() : $this->validateId($id);

        if ($request->session()->get('leadInserted')) {
            $data['alert'] = [
                'type'    => 'success',
                'message' => 'Lead criado com sucesso!'
            ];
        }

        return view('form', $data);
    }

    /**
     * "POST /lead/{id?}"
     *
     * Criar ou editar um lead
     *
     * @param $id
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function post(LeadFormRequest $request, $id = null)
    {
        $lead = $this->validateId($id);
        $input = $request->validated();

        $lead->nome     = $input['nome'];
        $lead->cpf      = $input['cpf'];
        $lead->telefone = $input['telefone'];
        $lead->email    = $input['email'];
        $lead->cep      = $input['cep'];
        $lead->rua      = $input['rua'];
        $lead->cidade   = $input['cidade'];
        $lead->estado   = $input['estado'];

        try {
            $lead->save();

            if ($lead->wasRecentlyCreated) {
                $request->session()->flash('leadInserted');
                return redirect('/lead/' . $lead['id']);
            }

            $alert = [
                'type' => 'success',
                'message' => 'Lead atualizado com sucesso!'
            ];
        } catch (\Exception $e) {
            $alert = ['type' => 'danger', 'message' => $e->getMessage()];
        }

        return view('form', ['lead' => $lead, 'alert' => $alert]);
    }

    /**
     * "DELETE /lead/{id}"
     *
     * Deletar um lead
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    public function delete($id)
    {
        $lead = $this->validateId($id);
        $lead->delete();

        return response()->json(['ok']);
    }

    /**
     * Retorna o Model de um lead existente ou um novo vazio
     *
     * @param $id
     *
     * @return Lead|\Illuminate\Database\Eloquent\Builder
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    private function validateId($id)
    {
        $lead = Lead::find($id);

        if (!$lead) {
            abort_if(isset($id), Response::HTTP_NOT_FOUND, 'Lead não encontrado');

            $lead = new Lead();
        }

        return $lead;
    }
}
