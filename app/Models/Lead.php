<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lead extends BaseModels
{
    use HasFactory;

    protected $fillable = ['nome', 'cpf', 'telefone', 'email', 'cep', 'rua', 'cidade', 'estado'];

    public $timestamps = false;
}
