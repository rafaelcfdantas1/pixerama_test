<?php

namespace Tests\Feature;

use App\Models\Lead;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LeadsTest extends TestCase
{
    use RefreshDatabase;

    private User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::find(1);
    }

    public function test_leads_page_contains_empty_leads_table(): void
    {
        Lead::truncate();

        $response = $this->actingAs($this->user)->get('/');

        $response->assertStatus(200);
        $response->assertSee('Nenhum lead cadastrado!');
    }

    public function test_leads_page_contains_non_empty_leads_table(): void
    {
        $this->createLead();

        $response = $this->actingAs($this->user)->get('/');

        $response->assertStatus(200);
        $response->assertDontSee('Nenhum lead cadastrado!');
    }

    public function test_user_can_see_lead_create_form(): void
    {
        $response = $this->actingAs($this->user)->get('/lead');

        $response->assertStatus(200);
    }

    public function test_user_can_see_lead_update_form(): void
    {
        $lead = $this->createLead();

        $response = $this->actingAs($this->user)->get('/lead/' . $lead->id);

        $response->assertStatus(200);
    }

    public function test_user_can_create_a_lead_with_adress(): void
    {
        $response = $this->actingAs($this->user)->post('/lead', [
            'nome' => fake()->name(),
            'cpf' => fake()->cpf(),
            'telefone' => fake()->phoneNumber(),
            'email' => fake()->safeEmail(),
            'cep' => fake()->postcode(),
            'rua' => fake()->streetName(),
            'cidade' => fake()->city(),
            'estado' => fake()->stateAbbr()
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('leadInserted');
    }

    public function test_user_can_create_a_lead_without_adress(): void
    {
        $response = $this->actingAs($this->user)->post('/lead', [
            'nome' => fake()->name(),
            'cpf' => fake()->cpf(),
            'telefone' => fake()->phoneNumber(),
            'email' => fake()->safeEmail(),
            'cep' => '',
            'rua' => '',
            'cidade' => '',
            'estado' => '',
        ]);

        $response->assertStatus(302);
        $response->assertSessionHas('leadInserted');
    }

    public function test_user_cannot_create_a_lead_with_cep_and_without_others_address_fields(): void
    {
        $response = $this->actingAs($this->user)->post('/lead', [
            'nome' => fake()->name(),
            'cpf' => fake()->cpf(),
            'telefone' => fake()->phoneNumber(),
            'email' => fake()->safeEmail(),
            'cep' => fake()->postcode(),
            'rua' => '',
            'cidade' => '',
            'estado' => '',
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors(['rua', 'cidade', 'estado']);
    }

    public function test_user_can_delete_existing_lead(): void
    {
        $lead = $this->createLead();

        $response = $this->actingAs($this->user)->delete('/lead/' . $lead->id);

        $response->assertStatus(200);
    }

    public function test_user_cannot_delete_non_existing_lead(): void
    {
        $response = $this->actingAs($this->user)->delete('/lead/1');

        $response->assertStatus(404);
    }

    private function createLead(): Lead
    {
        return Lead::factory()->create();
    }
}
