<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use RefreshDatabase;

    private User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::find(1);
    }

    public function test_registered_user_can_login(): void
    {
        $response = $this->post('/login', [
            'email' => 'admin@example.com',
            'password' => 'admin'
        ]);

        $response->assertStatus(302);
        $response->assertRedirect('/');
    }

    public function test_registered_user_can_see_leads_list(): void
    {
        $response = $this->actingAs($this->user)->get('/');

        $response->assertStatus(200);
    }

    public function test_registered_user_can_see_leads_form(): void
    {
        $response = $this->actingAs($this->user)->get('/lead');

        $response->assertStatus(200);
    }

    public function test_unregistered_user_cant_login(): void
    {
        $response = $this->post('/login', [
            'email' => 'unregistered@example.com',
            'password' => 'unregistered'
        ]);

        $response->assertStatus(302);
        $response->assertSessionHasErrors('user');
    }

    public function test_unregistered_user_cannot_see_leads_list(): void
    {
        $response = $this->get('/');

        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    public function test_unregistered_user_cannot_see_leads_form(): void
    {
        $response = $this->get('/lead');

        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }

    public function test_user_can_logout(): void
    {
        $response = $this->get('/logout');

        $response->assertStatus(302);
        $response->assertRedirect('/login');
    }
}
